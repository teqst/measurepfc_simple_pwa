/**
 *
 *
 * @author Tatevosyan Artem (@teqst)
 */

let storageData = localStorage.getItem('user-weight');

const notificationError = document.getElementById('notification-error');
const notificationSuccess = document.getElementById('notification-success');

const newValue = document.getElementById('new-weight');
const card = document.querySelector('.card');
const $userWeight = document.getElementById('user-weight');
const $newWeightButton = document.getElementById('create_new_weight');


$userWeight.append(createAddButton());

$newWeightButton.addEventListener('click', submitNewWeight);


//Functions

function createAddButton() {
    if (storageData) {
        card.addEventListener('click', (e) => {
            if (e.target !== newValue)
                card.classList.toggle('card--flipped');
        });

        return storageData + 'kg'
    }

    const addButton = document.createElement('i');
    addButton.className = 'card__button-add fas fa-plus-circle';
    addButton.setAttribute('id', 'card-add');

    addButton.addEventListener('click', () => {
        card.classList.toggle('card--flipped');
    })

    return addButton;
}



function submitNewWeight(e) {
    e.stopPropagation();

    if (newValue.value.length) {
        createUserLocalStorage(newValue.value);
        updateOldUserStorage(newValue.value);
        card.classList.toggle('card--flipped');
        newValue.value = '';
        notificationSuccess.querySelector('.notification__text').textContent = 'Your current weight parameter changed!';
        notificationSuccess.classList.add('active');
        setTimeout(() => {
            notificationSuccess.classList.remove('active');
        }, 3000);
        drawGraph();
        updateView();
    } else {
        notificationError.querySelector('.notification__text').textContent = 'You should enter your new weight..';
        notificationError.classList.add('active');
        setTimeout(() => {
            notificationError.classList.remove('active');
        }, 3000);
    }
}

function createUserLocalStorage(value) {
    localStorage.setItem('user-weight', value);
    storageData = localStorage.getItem('user-weight');
    $userWeight.textContent = value + 'kg';
}

function updateOldUserStorage(value) {
    const store = localStorage.getItem('user-old-weight');

    if (!store) {
        const values = [value];
        localStorage.setItem('user-old-weight', JSON.stringify(values));
    } else {
        const values = JSON.parse(localStorage.getItem('user-old-weight'));
        values.push(value);
        localStorage.setItem('user-old-weight', JSON.stringify(values));
    }
}