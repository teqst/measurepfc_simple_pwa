/**
 *
 *
 * @author Tatevosyan Artem (@teqst)
 */

const headerList = document.getElementById('header__list');
const burgerButton = document.getElementById('burger');

burgerButton.addEventListener('click', () => {
    headerList.classList.toggle('header__list--active');

    headerList.classList.contains('header__list--active') ?
        headerList.setAttribute('aria-expanded', true) :
        headerList.setAttribute('aria-expanded', false);

})