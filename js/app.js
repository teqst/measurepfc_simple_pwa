window.addEventListener('load', async () => {
    if (navigator.serviceWorker) {
        try {
            const regSw = await navigator.serviceWorker.register('/sw.js');
            console.log('SW Success', regSw);
        } catch (e) {
            console.log('SW Fail');
        }
    }

})
