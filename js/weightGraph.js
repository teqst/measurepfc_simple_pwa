/**
 *
 *
 * @author Tatevosyan Artem (@teqst)
 */

const canvas = document.getElementById('card-canvas');
const ctx = canvas.getContext('2d');
canvas.width = 900;

function getLocalStorageData() {
    return JSON.parse(localStorage.getItem('user-old-weight'));
}

function drawGraph() {
    const data = getLocalStorageData();
    if (!data) return;

    let cW = canvas.width;
    let cH = canvas.height;

    let pLen = data.length;
    let xLen = cW / pLen > 40 ? 40 : cW / pLen;

    ctx.clearRect(0, 0, cW, cH);
    ctx.beginPath();
    ctx.moveTo(0, cH);

    data.forEach((val, idx) => {
       ctx.strokeStyle = '#ffffff80';
       ctx.lineWidth = 4;
       ctx.lineTo(xLen * (idx + 1), cH - val);

       ctx.font = 'bold 15px Arial';
       ctx.fillStyle = '#2f2f2f';
       ctx.fillText(val,((xLen - 3) * (idx + 1)), (cH - val) - 17);
    });

    ctx.stroke();
    ctx.closePath();
}

drawGraph();