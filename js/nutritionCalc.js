/**
 *
 *
 * @author Tatevosyan Artem (@teqst)
 */

const pfcCard = document.getElementById('card-pfc');
const pfcValues = pfcCard.querySelectorAll("[id^=card-pfc__]");

const proteinPerKilo = 1.5, fatsPerKilo = .5, carbsPerKilo = 2.5;
const formulaArr = [proteinPerKilo, fatsPerKilo, carbsPerKilo];

function calculateNutrition() {
    let storageUserWeight = localStorage.getItem('user-weight');

    return formulaArr.map((val) => {
        return val * storageUserWeight;
    });
}

function updateView() {
    const values = calculateNutrition();

    pfcValues.forEach((cVal, i) => {
        cVal.textContent = values[i] + 'g';
    });
}

pfcCard.addEventListener('click', (e) => {
    pfcCard.classList.toggle('card--flipped');
});

updateView();